-- 1. Create DB, permissions etc.
-- Write a SQL script that creates a new user, and database. Make the user the owner of the db.

-- Create a new user
CREATE USER new_user WITH PAASWORD 'some_passwords'

-- Create a database
CREATE DATABASE ipl_db

-- Make user superuser 
ALTER ROLE new_user with superuser

--Make user the owner of the database
ALTER DATABASE ipl_db owner to new_user

-- 2. Clean up script
-- Write another SQL script that cleans up the user, and database created in the previous step.

--clean up database
DROP DATABASE ipl_db

--Clean up USER
DROP USER new_user

-- 3. Load CSV
-- Write a SQL script that loads CSV data into a table.

-- To load CSV data to a table, we need to have the same schema as the csv file in the table too,
-- so we need to create a table with the same schema as the csv file.

--Creating a table for matches.csv
CREATE TABLE matches (
        id serial PRIMARY KEY,
        season integer,
        city text,
        date date,
        team1 text,
        team2 text,
        toss_winner text,
        toss_decision text,
        result text,
        dl_applied integer,
        winner text,
        win_by_runs integer,
        win_by_wickets integer,
        player_of_match text,
        venue text,
        umpire1 text,
        umpire2 text
);

--Creating a table for deliveries.csv
CREATE TABLE deliveries (
 match_id integer,
 inning integer,
 batting_team text,
 bowling_team text,
 over integer,
 ball integer,
 batsman text,
 non_striker text,
 bowler text,
 is_super_over boolean,
 wide_runs integer,
 bye_runs integer,
 legbye_runs integer,
 noball_runs integer,
 penalty_runs integer,
 batsman_runs integer,
 extra_runs integer,
 total_runs integer,
 player_dismissed text,
 dismissal_kind text,
 fielder text
);

--Creating a table for umpires.csv
CREATE TABLE umpires (
 umpire VARCHAR(255),
 country VARCHAR(255)
);


--Now we have tables with the same schema as the csv files we need to load

--Copy command can be used here to load the csv file to an existing table
\copy deliveries from '/home/shaik/Downloads/csv_files/deliveries.csv' delimiter ',' csv header;
\copy matches from '/home/shaik/Downloads/csv_files/matches.csv' delimiter ',' csv header;
\copy umpires from '/home/shaik/Downloads/csv_files/umpires.csv' delimiter ',' csv header;
