
-- 1. Total runs scored by team
-- Query the total runs scored by each teams over the history of IPL. Hint: use the total_runs field.

select batting_team as Team , sum(total_runs) as Total_runs
from deliveries
group by batting_team;

-- 2. Top batsman for Royal Challengers Bangalore
-- Consider only games played by Royal Challengers Bangalore.
-- Now plot the total runs scored by top 10 batsman playing for Royal Challengers Bangalore over the history of IPL.

select batsman as player , sum(total_runs) as Runs
from deliveries
where batting_team='Royal Challengers Bangalore'
group by batsman
order by Runs desc
limit 10;

-- 3. Foreign umpire analysis
-- Obtain a source for country of origin of umpires.
-- Query chart of number of umpires by in IPL by country.
-- Indian umpires should be ignored as this would dominate the graph.

select country, count(umpire) from umpires
where country not like '%India%' group by country
order by country;

-- 4. Stacked chart of matches played by team by season
-- A stacked bar chart of ...
-- number of games played
-- by team
-- by season

select season,Team.team1, count(*)
from(
    select season,team1 from matches
    union all
    select season,team2 from matches
)as Team
group by season,Team.team1
order by season;

-- 5. Number of matches played per year for all the years in IPL.

select season,count(season)
from matches
group by season
order by season;

-- 6. Number of matches won per team per year in IPL.

select season,Team.winner, count(*)
from(
    select season,winner from matches
)as Team
group by season,Team.winner
order by season;

-- 7. Extra runs conceded per team in the year 2016

select batting_team, sum(extra_runs) as Total_Extra_Runs
from matches join deliveries
on matches.id=deliveries.match_id
where season='2016'
group by batting_team order by Total_Extra_Runs desc;

-- 8. Top 10 economical bowlers in the year 2015

select dv.bowler,sum(total_runs)/(count(dv.bowler)/6) as Economy_Rate
from deliveries as dv inner join matches on dv.match_id=matches.id
where matches.season=2015
group by dv.bowler order by Economy_Rate limit 10;