# Installation

## PostgreSql

- Follow the instructions given in this [article](https://www.digitalocean.com/community/tutorials/how-to-install-postgresql-on-ubuntu-20-04-quickstart) to successfully install postgresql in your system.
- After installation,
  - Create user,database.
  - Make sure to download [this](https://www.kaggle.com/datasets/manasgarg/ipl/) and [this](https://docs.google.com/spreadsheets/d/e/2PACX-1vQvjWx05eRuzqB13xREw_A7it43dgF4xJZMe87wtiwsFFSZWePB1l7M6G8BJllZ7ohiyZylPzouZrlI/pub?output=csv) csv file for the data.
  - And load this csv file to a new table.
  - Go through 'project_commands.sql' for all the commands.
  
- Now you can continue to run the scripts that are in the 'ipl_sql_ques.sql' file.
- Run scripts one by one to get the required sql dataset of the respective sceanarios.